﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace RabbitMQProducer
{
    class Program
    {//
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqps://ugipqojm:IJL3qtFFQxquxwoVXBt2W9HNgZiv5BD9@sparrow.rmq.cloudamqp.com/ugipqojm")
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("demo-queue",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            int counter = 0;
            string line = "";

            StreamReader file = new StreamReader(@"C:\Users\andre\Desktop\Tema 2\activity.txt");

            while((line = file.ReadLine()) != null)
            {
                var split = line.Split('\t');

                var startDate = DateTime.Parse(split[0]);
                var startDateInMillis = (long)(startDate - new DateTime(1970, 1, 1)).TotalMilliseconds;

                var endDate = DateTime.Parse(split[2]);
                var endDateInMillis = (long)(endDate - new DateTime(1970, 1, 1)).TotalMilliseconds;

                var data = new
                {
                    PatientId = "1",
                    Start = startDateInMillis,
                    End = endDateInMillis,
                    Activity = split[4]
                };
                
                System.Console.WriteLine(data.ToString());

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));

                channel.BasicPublish("", "demo-queue", null, body);

                Thread.Sleep(1000);

                counter++;
            }

            file.Close();
            System.Console.WriteLine("There were {0} lines", counter);

            System.Console.ReadLine();
        }
    }
}
